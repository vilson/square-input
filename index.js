;
(function() {
    if (window.Vue) {
        var style = 'border:none;background-color: rgba(0,0,0,0);color: rgba(0,0,0,0);-webkit-box-sizing: border-box;box-sizing:border-box;-webkit-user-select: none;-webkit-tap-highlight-color: transparent;outline: none;display:block;height:100%;width:100%;';
        var canvas = document.createElement('canvas');
        canvas.style.background = 'rgba(0,0,0,0)'
        var ctx=canvas.getContext('2d');
        var drawInput = function(ctx, x, y, w, h, focus, word, fontSize) {
          ctx.lineJoin = "round";  
          ctx.lineWidth = 2;  
          ctx.fillStyle = "rgba(0,0,0,0)";
          ctx.shadowBlur= 4;
          ctx.shadowOffsetX = 1; 
          ctx.shadowOffsetY = 1; 
          ctx.strokeStyle = focus ? "#00a4f5" : '#ccc';
          ctx.shadowColor=ctx.strokeStyle;
          ctx.strokeRect(x+5, y+5, w-10, h-10);
          if (word) {
            ctx.shadowBlur= 0;
            ctx.shadowColor = 'none';
            ctx.fillStyle = focus ? "#00a4f5" : '#666';
            ctx.font = fontSize + "px Courier New";
            ctx.fillText(word, x + (w - 0.6 * fontSize) / 2, y + (h + 0.55 * fontSize) / 2);
          }
        };
        var cursor = document.createElement('div');
        cursor.style.cssText = 'position:absolute;z-index:2';
        var count = 0;
        var opacitys = [1, 0.8, 0.5, 0.2, 0, 0, 0.4, 0.8, 1];
        var max = opacitys.length;
        setInterval(function() {
          count = count % max;
          cursor.style.opacity = opacitys[count];
          count++;
        }, 1000/10)
        var drawCursor = function(cursor, x, y, w, h, focus, word, fontSize) {
          cursor.style.background = '#00a4f5';
          cursor.style.width = fontSize / 9 + 'px';
          cursor.style.height = fontSize + 'px';
          cursor.style.left = x + (w - fontSize / 9) / 2 + (word ? 0.5 * fontSize : 0) + 'px';
          cursor.style.top = y + (h - fontSize) / 2 + 'px';
          return cursor;
        }

        Vue.component('x-words', {
            template: '<label style="position:relative;" :style="{backgroundImage: draw($el, size, index, code, focus)}"><input @focus="focus=true" @blur="focus=false" :maxlength="size" :type="type" v-model="code" style="' + style + '" /></label>',
            props: {
                size: {
                    type: Number,
                    default: 6
                },
                value: {
                    type: String,
                    default: ''
                },
                validate: {
                    type: [Function, RegExp],
                    default: function(word) {
                      return  /\d/.test(word);
                    }
                },
                type: {
                    type: String,
                    default: 'number'
                },
                drawInput: {
                  type: Function,
                  default: drawInput
                },
                drawCursor: {
                  type: Function,
                  default: drawCursor
                },
                autofocus: {
                  type: Boolean,
                  default: true
                }
            },
            data: function() {
              return {
                focus: false
              }
            },
            computed: {
              code: {
                get: function() {
                  return this.value;
                },
                set: function(value) {
                  var i = 0,
                      len = this.size,
                      word,
                      words = [];
                  for ( ; i < len ; i++ ) {
                      word = value ? value[i] || '' : '';
                      word = word ? this.test(word) ? word : '' : word;
                      words[i] = word;
                  }
                  words.sort(function(a, b) {
                      return !a && b;
                  });
                  this.$emit('input', words.join(''));
                  this.$nextTick(function() {
                    this.$forceUpdate();
                  });
                }
              },
              index: function() {
                return this.value.length;
              }
            },
            watch: {
              index: function(i) {
                if (i == this.size) {
                  this.$emit('full');
                }
              }
            },
            created: function() {
              this.code = this.value;
            },
            mounted: function() {
              if (this.autofocus) {
                this.$el.focus();
              }
            },
            methods: {
                test: function(word) {
                    return typeof this.validate == 'function' ? this.validate(word) : this.validate.test(word);
                },
                getCells: function($el, len, index, code, _focus) {
                  if (!$el) {
                    return ;
                  }
                  var cells =[], x, focus, i = 0;
                      width = $el.offsetWidth,
                      height = $el.offsetHeight,
                      space = (width - height * len)/(len - 1),                      
                      fontSize = getComputedStyle($el, false)['font-size'];
                  fontSize = fontSize.substring(0, fontSize.length - 2) * 1;
                  for (; i < len; i++) {
                    x = (space + height) * i;
                    focus = _focus ? index == i || ((i == len - 1) && (index == len)) : false;
                    cells.push([
                      x,
                      0,
                      height,
                      height,
                      focus,
                      code[i],
                      fontSize
                    ]);
                  }
                  return cells;
                },
                draw: function($el, len, index, code, focus) {
                    var cells = this.getCells($el, len, index, code, focus);
                    if (cells) {
                      if (!focus && (cursor.parentNode == $el)) {
                        $el.removeChild(cursor);
                      } else if (cursor.parent != $el) {
                        $el.appendChild(cursor);
                      }
                      var i = 0;
                      canvas.width = $el.offsetWidth;
                      canvas.height = $el.offsetHeight;
                      for (; i < len; i++) {
                        if (cells[i][4]) this.drawCursor.apply(this, [cursor].concat(cells[i]) );
                        this.drawInput.apply(this, [ctx].concat(cells[i]) );
                      }
                      var img = canvas.toDataURL('image/png');
                      return 'url(' + img + ')';
                    } 
                    return '';
                }
            }
        });
    }
})();