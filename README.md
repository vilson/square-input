square-input

#一个简单的vue小组件。重写了输入框和光标。把原有输入框隐藏。
只是把它弄到全局去了。代码也不长。如果要重写也不麻烦给。样式只给出了一个。可以重写
```
size //总方格数。（当然也可以是圆形的。）
value //默认值（当然这个值要先验证一下。）
validate //验证方法或者验证的正则
drawInput //画其中一个方格的方法
drawCursor //画光标的方法。
type //input的类型
autofocus //自动焦点
```
![demo](./demo.gif "demo")